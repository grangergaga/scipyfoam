## What's `sciPyFoam`

**sciPyFoam** is a python package for OpenFOAM 2D simulation results visualization. Although paraview, tecplot, ... provide a convenient way to visualize OpenFOAM results and do some post-processing, but Paraview can not generate high quality figures for publication purpose, tecplot sometimes is not so convenient to make figures. So sciPyFoam could help to generate very nice and complex figures. The main logics of sciPyFoam is using python to read native OpenFOAM polyMesh and results files (e.g. points, faces, ..., T, p) and then using VTK to convert polygonal cell data to triangle point data, finally using matplotlib to plot it.

**Here I provide a demo to illustrate how to use it.**

## Install

```bash
pip install sciPyFoam
```

## How to use ?

```py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import sciPyFoam.polyMesh2d as mesh2d
# Read data
caseDir='cases/blockMesh'  # case dir
fieldNames=['T','U']       # field name list
times,times_value=mesh2d.getTimes(caseDir)      # get all times name and value
MeshData=mesh2d.getMesh(caseDir, 'frontAndBack')
# read field data dict contains point data and cell data 
fieldData=mesh2d.readCellData_to_pointData(caseDir, times[-1], fieldNames,MeshData) 
# Plot 
fig=plt.figure(figsize=(14,6))
ax=plt.gca()
ax.tricontourf(MeshData['x'],MeshData['y'],MeshData['triangles'],fieldData['pointData']['T'],levels=50,cmap='rainbow')
# ax.invert_yaxis()
plt.tight_layout()
plt.savefig('test.pdf')
```

## Example

There is a [mini demo](example/MiniDemo.py) and [complex demo](example/plotField.py) could help you understand the usage.

1. Plot 2D regular mesh generated by blockMesh

```bash
cd example
python plotField.py cases/blockMesh latestTime T
```
![sciPyFoam plot blockMesh](./example/Mesh_blockMesh.jpg)

![sciPyFoam plot blockMesh](./example/T_blockMesh_1.jpg)

2. Plot 2D unstructured mesh generated by Gmsh

```bash
cd example
python plotField.py cases/triMesh latestTime T
```
![sciPyFoam plot blockMesh](./example/Mesh_triMesh.jpg)

![sciPyFoam plot blockMesh](./example/T_triMesh_1.jpg)

3. Plot 2D hybrid mesh generated by Gmsh

```bash
cd example
python plotField.py cases/hybridMesh latestTime T
```
![sciPyFoam plot blockMesh](./example/Mesh_hybridMesh.jpg)

![sciPyFoam plot blockMesh](./example/T_hybridMesh_1.jpg)

4. Plot 2D mesh generated by snappyHexMesh

```bash
cd example
python plotField.py cases/hybridMesh latestTime p
```
![sciPyFoam plot blockMesh](./example/Mesh_snappyHexMesh.jpg)

![sciPyFoam plot blockMesh](./example/p_snappyHexMesh_1.jpg)

```bash
cd example
python plotField.py cases/hybridMesh latestTime U
```

![sciPyFoam plot blockMesh](./example/U_snappyHexMesh_1.jpg)