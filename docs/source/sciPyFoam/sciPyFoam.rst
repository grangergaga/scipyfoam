sciPyFoam package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   sciPyFoam.postProcessing

Submodules
----------

sciPyFoam.figure module
-----------------------

.. automodule:: sciPyFoam.figure
   :members:
   :undoc-members:
   :show-inheritance:

sciPyFoam.polyMesh2d module
---------------------------

.. automodule:: sciPyFoam.polyMesh2d
   :members:
   :undoc-members:
   :show-inheritance:

sciPyFoam.readvtk module
------------------------

.. automodule:: sciPyFoam.readvtk
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: sciPyFoam
   :members:
   :undoc-members:
   :show-inheritance:
